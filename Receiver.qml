import QtQuick 2.0

Circle {
    id: receiveButton
    width: 150
    height: 150

    function receive(value) {
        displayText = value
        clickNotify.running = true
    }

    SequentialAnimation on buttonColor {
        id: clickNotify
        running: false

        ColorAnimation {
            from: "black"
            to: "white"
            duration: 200
        }
        ColorAnimation {
            from: "white"
            to: "black"
            duration: 200
        }
    }
}
