import QtQuick 2.0

Circle {
    id: sender
    width: 150
    height: 150

    property int count: 0
    property Receiver target: null

    signal send(string value)
    onTargetChanged: send.connect(target.receive)

    MouseArea {
        anchors.fill: parent
        onClicked: {
            count++
            parent.send(count)
        }

        onPressed: parent.buttonColor = "lightGreen"
        onReleased: parent.buttonColor = "Green"
    }
}
