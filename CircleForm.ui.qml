import QtQuick 2.4

Item {
    width: 400
    height: 400
    property alias displayText: display.text
    property alias buttonColor: button.color

    Rectangle {
        id: button
        color: "#161212"
        radius: width * 0.5
        anchors.fill: parent

        Text {
            id: display
            x: 203
            y: 169
            color: "#ffffff"
            text: qsTr("Text")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family: "Verdana"
            style: Text.Sunken
            font.bold: true
            font.pixelSize: 20
        }
    }
}


/*##^## Designer {
    D{i:1;anchors_height:200;anchors_width:200;anchors_x:77;anchors_y:168}
}
 ##^##*/
