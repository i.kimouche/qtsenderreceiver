import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    id: window
    visible: true
    width: 400
    height: 250
    title: qsTr("Sender & Receiver")

    Background {
        id: background
        anchors.fill: parent
    }

    Sender {
        id: sender
        y: 50
        displayText: "SENDER"
        buttonColor: "#008000"
        anchors.left: parent.left
        anchors.leftMargin: 20
        target: receiver
    }

    Receiver {
        id: receiver
        x: 235
        y: 50
        buttonColor: "#ff0000"
        displayText: "RECEIVER"
        anchors.right: parent.right
        anchors.rightMargin: 20
    }

}

/*##^## Designer {
    D{i:1;anchors_x:99;anchors_y:48}D{i:3;anchors_x:19}
}
 ##^##*/
